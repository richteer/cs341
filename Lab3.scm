(define make-window
  (lambda (x-size y-size foreground-color background-color)
    (let ((window (make-graphics-device 'x)))				; this is for UNIX 
	(begin
	  (graphics-set-coordinate-limits window 0 0 x-size y-size)
	  (set-foreground-color window foreground-color)
	  (set-background-color window background-color)
	  (graphics-clear window)
	  window))))

(define kill-window
  (lambda (window)
    (graphics-close window)))

(define set-foreground-color
  (lambda (window color)
    (graphics-operation window 'set-foreground-color color)))

(define set-background-color
  (lambda (window color)
    (graphics-operation window 'set-background-color color)))

(define draw-point
  (lambda (window x1 y1)
    (graphics-operation window 'draw-point x1 y1)))

(define draw-line
  (lambda (window x1 y1 x2 y2)
    (graphics-operation window 'draw-line x1 y1 x2 y2)))

(define draw-ellipse
  (lambda (window x-left y-top x-right y-bottom)
    (graphics-operation window 'draw-ellipse x-left y-top x-right y-bottom)))

(define draw-circle
  (lambda (window x-center y-center radius)
    (graphics-operation window 'draw-circle x-center y-center radius)))

(define draw-polygon
  (lambda (window vector-points)
    (graphics-operation window 'fill-polygon vector-points)))

;------ Start My Functions: 

(define draw-square
	(lambda (window x y size)
		(draw-line window (- x size) (- y size) (- x size) (+ y size))
		(draw-line window (- x size) (- y size) (+ x size) (- y size))
		(draw-line window (+ x size) (- y size) (+ x size) (+ y size))
		(draw-line window (+ x size) (+ y size) (- x size) (+ y size))
		))

(define random-squares
	(lambda (num)
		(letrec ((helper (lambda (ls n)
			(cond ((equal? n 0) ls)
				  (else (helper (cons (list (+ 10 (random 480))
				  							(+ 10 (random 480))
				  							20) ls)
				  				(- n 1)))))))
			(helper '() num))))		

(define square-check 
	(lambda (x y ls)
		(cond ((null? ls) (list x y))
			  ((and (<= (- (cadar ls) (caddar ls)) y (+ (cadar ls) (caddar ls))) (equal? x (+ (caar ls) (caddar ls) )) (list (+ x 1) y)))
			  ((and (<= (- (cadar ls) (caddar ls)) y (+ (cadar ls) (caddar ls))) (equal? x (- (caar ls) (caddar ls) )) (list (- x 1) y)))
			  ((and (<= (- (caar ls) (caddar ls)) x (+ (caar ls) (caddar ls))) (equal? y (+ (cadar ls) (caddar ls) )) (list x (+ y 1))))
			  ((and (<= (- (caar ls) (caddar ls)) x (+ (caar ls) (caddar ls))) (equal? y (- (cadar ls) (caddar ls) )) (list x (- y 1))))
		  	  (else (square-check x y (cdr ls))))))


(define fix-bounds
	(lambda (x y squares)
		(cond ((> x 500) (list 500 y))
			  ((> y 500) (list x 500))
			  ((< x 0) (list 0 y ))
			  ((< y 0) (list x 0 ))
			  (else (square-check x y squares)))))


(define random-color
	(lambda ()
		 ; Edit the below list to add more colors
		(let ((clrs '("red" "white" "blue")))
		(letrec (
				 (rnd (random (length clrs)))
				 (helper (lambda (x ls)
				 	(cond ((equal? 0 x) (car ls))
				 		  (else (helper (- x 1) (cdr ls)))))))
			(helper rnd clrs)))))

(define fun
	(lambda (x y window squares)
		(let ((coord (fix-bounds x y squares)))
			(let ((dx (car coord)) (dy (cadr coord)))
			(set-foreground-color window (random-color)) 
			(draw-point window dx dy)
			(cond ((and (equal? dx 500) (equal? dy 500)) 0)
				  (else (let ((rnd (random 4)))
			  		(fun (+ dx (- (modulo rnd 2) (* (modulo rnd 2) (* 2 (floor (/ rnd 2))))))
			  			 (+ dy (- (modulo (+ 1 rnd) 2) (* (modulo (+ 1 rnd) 2) (* 2 (floor (/ rnd 2))))))
			  		 	  window squares))))))))

(define test
	(lambda ()
		(letrec ((window (make-window 500 500 "brown" "black"))
				 ; Change the number of random houses to generate
				 (houses (random-squares 10))
				 (h-house (lambda (h-ls)
				 	(cond ((null? h-ls))
				 		  (else (draw-square window (caar h-ls) (cadar h-ls) (caddar h-ls)) (h-house (cdr h-ls)))))))
			(h-house houses)
			(fun 0 0 window houses)
			(kill-window window)
			"Success!"
			)))
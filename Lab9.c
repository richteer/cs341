#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERROR  1
#define NUMBER 2
#define STRING 4
#define CONCAT 8
#define REPEAT 16
#define PAREN  32
#define OP (CONCAT | REPEAT)

#define HERE(A) (verbose&&fprintf(stderr,"Getting here: %d\n",A))
#define HERES(A) (verbose&&fprintf(stderr,"Getting here: %s\n",A))

typedef struct token_s {
	char* body;	// The actual string
	int size;	// Size of the string buffer
	int length;	// How many characters are in the buffer
	char  type;	// What kind of token this is
} token_t;

typedef struct value_s {
	char* body;
	int size;
	int length;
} value_t;

int get_token(char);

value_t eval(); 
value_t term();
value_t last();

int addTo(value_t*,value_t*);

int isstring(int c) { return isgraph(c) && (c != '^') && (c != '.') && (c != '(') && (c != ')'); }

static void pull_token(token_t *tok, int (* isUseful)(int));

static void set_body(token_t *tok,const char* buf);

static int deltoken(token_t *tok);
static int ptoken(token_t *tok);

char verbose = 0;


token_t* curToken(token_t *tok)
{
	static token_t thetoken;
	(tok) && memcpy(&thetoken,tok,sizeof(token_t));
	
	return &thetoken;
}

int main(int argc, char** argv)
{
	char* result;
	value_t val;

	((argc >> 1) && (!strncmp(argv[1],"-v",3))) &&
		(fprintf(stderr,"Verbose mode enabled!\n") &&
		(verbose = 1));

	printf(">>> ");
	fflush(stdout);
	get_token(~OP);
	val = eval();
	printf("%s\n",val.body);
	
	return main(0,NULL);
}

value_t eval()
{
	HERES("Entering eval");
	value_t val = term(curToken(NULL));
	value_t other;

	HERE(curToken(NULL)->type);

//	(curToken(NULL)->type & CONCAT) && (get_token(~OP)), addTo(&val,curToken(NULL)), 

	(curToken(NULL)->type == CONCAT) && (get_token(~OP), other = eval(), addTo(&val,&other)); 
	HERES("Exiting eval");
	HERE(curToken(NULL)->type);
	return val;
}

value_t term()
{
	HERES("Entering term");
	value_t val = last(curToken(NULL));
	value_t other;

	(curToken(NULL)->type == REPEAT) &&  
		((get_token(NUMBER|PAREN),1) ?
			(other = last(), addNumTime(&val,&other))
		:	
			(fprintf(stderr,"Error using '^' operator, not a number\n"),exit(1),1));

	HERES("Exiting term");
	return val;
}


value_t last()
{
	HERES("Entering last");
	value_t val = {0};

	//printf("%d\n",curToken(NULL)->type);

	HERES("Exiting last");
	return (curToken(NULL)->type & (STRING|NUMBER)) ? 
		(val = *((value_t*) curToken(NULL)),
		get_token(OP|PAREN),
		val)
		:
		((curToken(NULL)->type & (PAREN)) ?
			(get_token(~OP),(val = eval()), get_token(OP), val)
			: val);
			
}



int addTo(value_t *val, value_t *tok)
{
	
	((val->length += tok->length) > (val->size + 1)) && (val->body = realloc(val->body,val->size <<= 1));

	strcat(val->body,tok->body);

	return 0;
}

int addNumTime(value_t *val, value_t *oth)
{
	static value_t base = {0};

	int ret;
	(base.body == NULL) && memcpy(&base,val,sizeof(value_t)) && strcpy((base.body = (char*) calloc(1,val->length+1)),val->body);

	((val->length + base.length) > (val->size+1)) && (val->body = realloc(val->body,val->size <<= 1));

	

	(strDec(oth)) && 
		strcat(val->body,base.body) &&
		(val->length += base.length) && addNumTime(val,oth);

	free(base.body);
	base.body = NULL;
	return 1;	
}

int stringReverse(char* body, int length, int cur)
{
	static char* ret;
	(ret == NULL) && (ret = calloc(1,length+1)) && (cur = length);

	(cur+1) && (ret[length-cur] = body[cur]) && stringReverse(body,length,cur-1);
	
	strcpy(body,ret);
	free(ret);

	return 1;	
}

int strDec(value_t *val)
{
	int i = val->length - 1;
	(val->body[i] == '0' && i > 0) ? (
		val->body[i] = '0',
		strDec(val),
		val->body[i] = '9'
	) : (
		val->body[i] != '0' && val->body[i]--
	);

	return !isZeros(val->body, val->length-1);
}

int isZeros(char *val, int cur)
{

	return (cur+1) ? ('0' == val[cur]) && isZeros(val,cur-1) : 1;
}

int get_token(char expect)
{
	int c;
	token_t token = {0};
	token_t *tok = &token;

again:

	switch (c = getchar()) {
		case '.':
			set_body(tok,".");
			tok->type = CONCAT;
			break;
		case '^':
			set_body(tok,"^");
			tok->type = REPEAT;
			break;
		case '(':
			set_body(tok,"(");
			tok->type = PAREN;
			break;
		case ')':
			set_body(tok,")");
			tok->type = PAREN;
			break;
		case EOF:
		case '\n':
			verbose && fprintf(stdout,"\n");
			return 0;
		case ' ':
		case '\t':
			goto again;
		default:
			ungetc(c,stdin);
			pull_token(tok,( (isdigit(c)) ? ((tok->type = NUMBER),isdigit) : ((tok->type = STRING),isstring) ));
			break;
	}

	expect && !(expect & tok->type) && fprintf(stderr,"\nERROR: Expected %d, got %d\n",expect,tok->type) && (exit(1),1);
	curToken(tok);
	verbose && ptoken(tok);
	return 1;
}

// Pull in a string until a whitespace or new operator appears
static void pull_token(token_t *tok, int (* isUseful)(int) )
{
	char c;
	// First runthrough
	(NULL == tok->body) && (tok->body = (char*) malloc (tok->size = 2));

	// If the body needs to be expanded...
	((tok->size - 1) == tok->length) && (tok->body = (char*) realloc(tok->body,tok->size <<= 1)); // ...do so


	// Base case, return when the next character is not useful
	(!isUseful(c = getchar())) ?
		(ungetc(c,stdin),
		(tok->body[tok->length] = '\0')) // Null terminate the string
	:
		(
		(tok->body[tok->length++] = c),
		pull_token(tok,isUseful)
		);
	return;
}

static void set_body(token_t *tok, const char* buf)
{
	// I'm sorry...
	strcpy((tok->body = (char*) calloc(1,tok->size = (tok->length = strlen(buf)) + 1)),buf);
	/* Calculates the length of the passed in string,
		set the token length to the string length,
		set the token buffer size to the length + 1,
		allocates memory based on the size, (and sets it to zero)
		and copies the buffer into the new allocated space. */

	return;
}

static int deltoken(token_t *tok)
{
	return free(tok->body),tok->size = tok->length = tok->type = 0;
}

static int ptoken(token_t *tok)
{
	switch(tok->type) {
		case CONCAT:
		case REPEAT:
			fprintf(stderr,"[OP:%c]\n",(tok->type == CONCAT) ? '.' : '^');
			break;
		case STRING:
		case NUMBER:
			fprintf(stderr,"[%s:\"%s\"]\n",(tok->type == STRING) ? "STRING" : "NUMBER",tok->body);
			break;
		case PAREN:
			fprintf(stderr,"[PAREN:\"%c\"]\n",tok->body[0]);
			break;
		default:
			fprintf(stderr,"[ERROR:\"%s\"]\n",tok->body);
			break;
	}

	return 0;
}


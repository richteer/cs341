#lang racket
(define my-length
  (lambda (ls)
    (letrec ((ans 0) (helper (lambda (l ans)
                               (cond ((null? l) ans)
                                     (else (helper (cdr l) (+ 1 ans)))))))
      (helper ls ans))))

(define my-append
  (lambda (ls1 ls2)
    (letrec ((helper (lambda (ls1 ans)
                       (cond ((null? ls1) ans)
                             (else (cons (car ls1) (helper (cdr ls1) ans)))))))
            (helper ls1 ls2))))

(define my-reverse
  (lambda (ls)
    (letrec ((helper (lambda (ls ans)
      (cond ((null? ls) ans)
             (else (helper (cdr ls) (cons (car ls) ans)))))))
      (helper ls '()))))

;-----------------------------------;

(define prefix?
  (lambda (ls1 ls2)
    (letrec ((helper (lambda (ls1 ls2 ans)
                       (cond ((null? ls1) ans)
                             ((null? ls2) #f)
                             (else (helper (cdr ls1) 
                                           (cdr ls2) 
                                           (and (equal? (car ls1) (car ls2)) ans)))))))
      (helper ls1 ls2 #t))))

(define subsequence?
  (lambda (ls1 ls2)
    (if (null? ls1) #t
        (letrec ((helper (lambda (ls1 ls2 ans)
                       (cond ((null? ls1) ans)
                             ((null? ls2) ans)
                             (else (helper ls1 
                                           (cdr ls2)
                                           (or (prefix? ls1 ls2) ans)))))))
          (helper ls1 ls2 #f)))))

(define sublist?
  (lambda (ls1 ls2)
    (letrec ((h-exist? (lambda (item ls ans)
                         (cond ((null? ls) ans)
                               (else (h-exist? item (cdr ls) (or 
                                                              (equal? item (car ls))
                                                              ans))))))
             (h-sub (lambda (ls1 ls2 ans)
                       (cond ((null? ls1) ans)
                             ((null? ls2) ans)
                             (else (h-sub (cdr ls1) ls2 (and ans
                                                             (h-exist? (car ls1) ls2 #f))))))))
      (h-sub ls1 ls2 #t))))

;--------------------------------------;

(define map
  (lambda (f ls)
    (letrec ((helper (lambda (ls ans)
                       (cond ((null? ls) ans)
                             (else (helper (cdr ls ) (cons (f (car ls)) ans)))))))
      (reverse (helper ls '())))))

(define filter
  (lambda (test ls)
    (letrec ((helper (lambda (ls ans)
                       (cond ((null? ls) ans)
                             (else (helper (cdr ls) (if (test (car ls)) (cons (car ls) ans) ans)))))))
      (reverse (helper ls '())))))

;------------------------------------------;
; Inserts the item into the sorted list by the comparator provided
(define insert
  (lambda (item list compare)
    (letrec ((helper (lambda (front back)
                       (cond ((null? back) (append (reverse front) (cons item '())))
                             ((compare (car back) item) (helper (cons (car back) front)
                                                                (cdr back)) ) 
                             (else (append (reverse front) (cons item back)))))))
      (helper '() list))))

(define num-sort
  (lambda (ls)
    (letrec ((helper (lambda (ls ans)
                       (cond ((null? ls) ans)
                             (else (helper (cdr ls) (insert (car ls) ans <)))))))
      (helper ls '()))))

(define sort
  (lambda (ls cmp)
    (letrec ((helper (lambda (ls ans)
                       (cond ((null? ls) ans)
                             (else (helper (cdr ls) (insert (car ls) ans cmp)))))))
      (helper ls '()))))

(define make-sort
  (lambda (cmp)
    (lambda (ls)
      (letrec ((helper (lambda (ls ans)
                         (cond ((null? ls) ans)
                               (else (helper (cdr ls) (insert (car ls) ans cmp)))))))
        (helper ls '())))))
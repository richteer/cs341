#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define NUMBER 0
#define OPERATOR 1

int current_token;
int current_attribute;

int expr(), term(), factor();
void match(int), error();

int main()
{
	int c;

	fprintf(stderr,">>> ");
	current_token = get_token();
	value = expr();
	if(current_token != '\n')
	{
		fprintf(stderr,"\nThere was an error in parsing\n");
		return 1;
	}
	fprintf(stderr, "\nNo errors: %d\n",value);
}

int expr()
{
	int value = term();
	while (current_token == '+') 
	{
		match('+');
		value += term();
	}

	return value;
}

int term()
{
	factor()
}

int factor()
{
	int value;
	if (current_token == '(')
	{
		match('(');
		value = expr();
		match(')');
	}
	else if (current_token == NUMBER)
	{
		value = current_attribute;
		match(NUMBER);
	}
	else 
	{
		error();
	}
	return value;
}


void match(int token)
{
	if (current_token == token)
	{
		current_token = get_token();
	}
	else
	{

	}
}

int get_token() 
{
	int c = 0;

	while (1) 
	{
		switch(c = getchar())
		{
			case '+':
			case '*':  fprintf(stderr,"[OP:%c]",c); return OPERATOR;
			case '(':
			case ')':  fprintf(stderr,"[SPECIAL:%c]",c);      return c;
			case ' ':
			case '\t':                              continue; 
			case '\n': fprintf(stderr,"%c",c);      return c;
			default:
				if (isdigit(c))
				{
					value = 0;
					do {
						value = 10*value + (c - '0');
					}
					while (isdigit(c = getchar()));
					ungetc(c,stdin);
					current_attribute = value;
					fprintf(stderr,"[NUM:%d]",current_attribute);
					return NUMBER;
				}
				else 
				{
					fprintf(stderr,"[ERROR:%c]",c);
					return c;
				}
		}
	}
}

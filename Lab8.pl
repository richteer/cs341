% my_delete(+Item,+List,-Answer)
% Deletes all occurrences of Item from List and returns the result in Answer. (needed for #1)
%
my_delete(_,[],[]).
my_delete(A,[A|As],Bs) :- my_delete(A,As,Bs).
my_delete(A,[B|As],[B|Bs]) :- \+ A=B, my_delete(A,As,Bs).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

% remove_duplicates1(+List1,-List1)
% Removes duplicates from List1 and returns answer in List2 without member function
%
remove_duplicates1([],[]).
remove_duplicates1([A|As],[A|Bs]) :- my_delete(A,As,Ats), remove_duplicates1(Ats,Bs).


% remove_duplicates2(+List1,-List2)
% Removes duplicates from List1 and returns answer in List2 using the member function
% 
remove_duplicates2([],[]).
remove_duplicates2([A|As],[A|Bs]) :- remove_duplicates2(As,Bs), \+ member(A,Bs).
remove_duplicates2([A|As],Bs) :- remove_duplicates2(As,Bs), member(A,Bs).


% suffix1(+List1,+List2)
% Checks if List1 is a suffix of List2 (using prefix)
%
suffix1(As,Bs) :- reverse(As,Ars), reverse(Bs,Brs), prefix(Ars,Brs).


% suffix2(+List1,+List2)
% Checks if List1 is a suffix of List2 (without using prefix, two-liner)
%
suffix2(As,Bs) :- As=Bs.
suffix2(As,[_|Bs]) :- suffix2(As,Bs).

% suffix3(+List1,+List2)
% Checks if List1 is a suffix of List2 (without prefix, one-liner)
%
suffix3(As,Bs) :- append(_,As,Bs).